package hvktqs.k27.watermarking;

import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    public native double[] getSignature();

    public native void getSignatureFile();

    public native void encodeImage();

    static {
        System.loadLibrary("watermarking-lib");
        System.loadLibrary("watermarking-encode-lib");
    }
}

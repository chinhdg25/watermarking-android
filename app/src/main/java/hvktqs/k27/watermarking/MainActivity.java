package hvktqs.k27.watermarking;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String text = "";
        double[] sigs = getSignature();
        if (sigs != null) {
            for (double sig : sigs) {
                text += sig + "\n";
            }
        }
        TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(text);

        getSignatureFile();
    }
}

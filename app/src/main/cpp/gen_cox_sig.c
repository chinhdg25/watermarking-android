#include <time.h>
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "wm.h"

jdoubleArray *Java_hvktqs_k27_watermarking_MainActivity_getSignature2(JNIEnv *env, jobject obj) {
    int n = 100, i;
    jdouble a = 0.3;
    jdouble m = 0.0;
    jdouble d = 1.0;
    jdouble list[n + 4];

    srandom(time(NULL) * getpid());

    list[0] = n;
    list[1] = a;
    list[2] = m;
    list[3] = d;
    for (i = 4; i < n + 4; i++) {
        jdouble x;
        jdouble x1, x2;
        do {
            x1 = 2.0 * ((random() & RAND_MAX) / ((jdouble) RAND_MAX + 1.0)) - 1.0;
            x2 = 2.0 * ((random() & RAND_MAX) / ((jdouble) RAND_MAX + 1.0)) - 1.0;
            x = x1 * x1 + x2 * x2;
        } while (x >= 1.0);
        x1 *= sqrt((-2.0) * log(x) / x);
        x2 *= sqrt((-2.0) * log(x) / x);
        list[i] = m + d * x1;
        list[i + 1] = m + d * x2;
        i++;
    }
    jdoubleArray outJNIArray = (*env)->NewDoubleArray(env, n + 4);
    if (NULL == outJNIArray) {
        return NULL;
    }
    (*env)->SetDoubleArrayRegion(env, outJNIArray, 0, n + 4, list);
    return outJNIArray;
}

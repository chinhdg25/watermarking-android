#include <jni.h>
#include <string>

extern "C"
jstring
Java_hvktqs_k27_watermarkingincludec_MainActivity_stringFromJNI2(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++ 2";
    return env->NewStringUTF(hello.c_str());
}

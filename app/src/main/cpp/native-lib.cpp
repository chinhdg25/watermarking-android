#include <jni.h>
#include <string>

extern "C"
jstring Java_hvktqs_k27_watermarking_MainActivity_stringFromJNI(JNIEnv *env, jobject) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

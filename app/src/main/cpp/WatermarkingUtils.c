#include <time.h>
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "wm.h"

jdoubleArray *Java_hvktqs_k27_watermarking_BaseActivity_getSignature(JNIEnv *env, jobject obj) {
    int n = 100, i;
    jdouble a = 0.3;
    jdouble m = 0.0;
    jdouble d = 1.0;
    jdouble list[n + 4];

    srandom(time(NULL) * getpid());

    list[0] = n;
    list[1] = a;
    list[2] = m;
    list[3] = d;
    for (i = 4; i < n + 4; i++) {
        jdouble x;
        jdouble x1, x2;
        do {
            x1 = 2.0 * ((random() & RAND_MAX) / ((jdouble) RAND_MAX + 1.0)) - 1.0;
            x2 = 2.0 * ((random() & RAND_MAX) / ((jdouble) RAND_MAX + 1.0)) - 1.0;
            x = x1 * x1 + x2 * x2;
        } while (x >= 1.0);
        x1 *= sqrt((-2.0) * log(x) / x);
        x2 *= sqrt((-2.0) * log(x) / x);
        list[i] = m + d * x1;
        list[i + 1] = m + d * x2;
        i++;
    }
    jdoubleArray outJNIArray = (*env)->NewDoubleArray(env, n + 4);
    if (NULL == outJNIArray) {
        return NULL;
    }
    (*env)->SetDoubleArrayRegion(env, outJNIArray, 0, n + 4, list);
    return outJNIArray;
}


void Java_hvktqs_k27_watermarking_BaseActivity_getSignatureFile(JNIEnv *env, jobject obj) {
    FILE *out = stdout;
    char output_name[MAXPATHLEN] = "/sdcard/cox.sig";
    int n = 100, i;
    double a = 0.3;
    double m = 0.0;
    double d = 1.0;
    double list[n + 4];
    out = fopen(output_name, "w");
    srandom(time(NULL) * getpid());

    list[0] = n;
    list[1] = a;
    list[2] = m;
    list[3] = d;
    for (i = 4; i < n + 4; i++) {
        double x;
        double x1, x2;
        do {
            x1 = 2.0 * ((random() & RAND_MAX) / ((double) RAND_MAX + 1.0)) - 1.0;
            x2 = 2.0 * ((random() & RAND_MAX) / ((double) RAND_MAX + 1.0)) - 1.0;
            x = x1 * x1 + x2 * x2;
        } while (x >= 1.0);
        x1 *= sqrt((-2.0) * log(x) / x);
        x2 *= sqrt((-2.0) * log(x) / x);
        list[i] = m + d * x1;
        list[i + 1] = m + d * x2;
        i++;
    }
    for (i = 0; i < n + 4; i++) {
        fprintf(out, "%f\n", list[i]);
    }
    fclose(out);
}

//void Java_hvktqs_k27_watermarking_BaseActivity_getSignatureFile2(JNIEnv *env, jobject obj) {
//    printf("init_main\n");
//    FILE *in, *out, *sig;
//
//    char output_name[MAXPATHLEN] = "cox_zi2.pgm";
//    char input_name[MAXPATHLEN] = "lena.pgm";
//    char signature_name[MAXPATHLEN] = "cox.sig";
//
//    int row, i, j, n, rows, cols, format;
//    double alpha = 0.0;
//    double threshold, *largest, **dcts;
//    gray **input_image, **output_image, maxval;
//
//    wm_init();
//
//    out = fopen(output_name, "wb");
//    sig = fopen(signature_name, "r");
//    in = fopen(input_name, "rb");
//
//    if (sig) {
//        fscanf(sig, "%d\n", &n);
//        if (alpha == 0.0)
//            fscanf(sig, "%lf\n", &alpha);
//        else
//            fscanf(sig, "%*f\n");
//        fscanf(sig, "%*f\n");
//        fscanf(sig, "%*f\n");
//    }
//    else {
//        fprintf(stderr, "signature file not specified, use -s file option\n");
//        exit(1);
//    }
//
//    pgm_readpgminit(in, &cols, &rows, &maxval, &format);
//
//    init_dct_NxN(cols, rows);
//
//    dcts = alloc_coeffs(cols, rows);
//    input_image = pgm_allocarray(cols, rows);
//
//    for (row = 0; row < rows; row++)
//        pgm_readpgmrow(in, input_image[row], cols, maxval, format);
//
//    fclose(in);
//
//    output_image = pgm_allocarray(cols, rows);
//
//    fdct_NxN(input_image, dcts);
//
//    largest = malloc((n + 1) * sizeof(double));
//    select_largest_coeffs(dcts[0], cols * rows, n + 1, largest);
//    threshold = largest[0];
//    free(largest);
//
//    j = 0;
//    for (i = 0; i < n; i++) {
//        double v;
//
//        while (dcts[j / cols][j % cols] < threshold) j++;
//
//        fscanf(sig, "%lf\n", &v);
//        dcts[j / cols][j % cols] *= (1.0 + alpha * v);
//        j++;
//    }
//
//    idct_NxN(dcts, output_image);
//    free_coeffs(dcts);
//
//    pgm_writepgminit(out, cols, rows, maxval, 0);
//
//    for (row = 0; row < rows; row++)
//        pgm_writepgmrow(out, output_image[row], cols, maxval, 0);
//
//    fclose(out);
//
//    fclose(sig);
//
//    pgm_freearray(output_image, rows);
//    pgm_freearray(input_image, rows);
//}
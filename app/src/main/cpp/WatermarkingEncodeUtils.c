#include <time.h>
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include "wm.h"
#include "dct.h"
#include "sort.h"

void Java_hvktqs_k27_watermarking_BaseActivity_encodeImage(JNIEnv *env, jobject obj) {
    printf("init_main\n");
    FILE *in, *out, *sig;

    char output_name[MAXPATHLEN] = "cox_zi2.pgm";
    char input_name[MAXPATHLEN] = "lena.pgm";
    char signature_name[MAXPATHLEN] = "cox.sig";

    int row, i, j, n, rows, cols, format;
    double alpha = 0.0;
    double threshold, *largest, **dcts;
    gray **input_image, **output_image, maxval;

    wm_init();

    out = fopen(output_name, "wb");
    sig = fopen(signature_name, "r");
    in = fopen(input_name, "rb");

    if (sig) {
        fscanf(sig, "%d\n", &n);
        if (alpha == 0.0)
            fscanf(sig, "%lf\n", &alpha);
        else
            fscanf(sig, "%*f\n");
        fscanf(sig, "%*f\n");
        fscanf(sig, "%*f\n");
    }
    else {
        fprintf(stderr, "signature file not specified, use -s file option\n");
        exit(1);
    }

    pgm_readpgminit(in, &cols, &rows, &maxval, &format);

    init_dct_NxN(cols, rows);

    dcts = alloc_coeffs(cols, rows);
    input_image = pgm_allocarray(cols, rows);

    for (row = 0; row < rows; row++)
        pgm_readpgmrow(in, input_image[row], cols, maxval, format);

    fclose(in);

    output_image = pgm_allocarray(cols, rows);

    fdct_NxN(input_image, dcts);

    largest = malloc((n + 1) * sizeof(double));
    select_largest_coeffs(dcts[0], cols * rows, n + 1, largest);
    threshold = largest[0];
    free(largest);

    j = 0;
    for (i = 0; i < n; i++) {
        double v;

        while (dcts[j / cols][j % cols] < threshold) j++;

        fscanf(sig, "%lf\n", &v);
        dcts[j / cols][j % cols] *= (1.0 + alpha * v);
        j++;
    }

    idct_NxN(dcts, output_image);
    free_coeffs(dcts);

    pgm_writepgminit(out, cols, rows, maxval, 0);

    for (row = 0; row < rows; row++)
        pgm_writepgmrow(out, output_image[row], cols, maxval, 0);

    fclose(out);

    fclose(sig);

    pgm_freearray(output_image, rows);
    pgm_freearray(input_image, rows);

}